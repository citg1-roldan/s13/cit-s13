package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//allows us to use crud repository methods
@Service
public class PostServiceImpl implements PostService {

    //@autowired allows us to use an inteface as if it was an instance of an object
    @Autowired
    private PostRepository postRepository;
    public void createPost(Post post) {
        postRepository.save(post);
    }
    public Iterable<Post> getPosts(){
        return postRepository.findAll();
    }

    @Override
    public ResponseEntity deletePost(Long id) {
        postRepository.deleteById(id);

        return new ResponseEntity<>("Post Deleted Succesfully", HttpStatus.OK);
    }

    @Override
    public ResponseEntity updatePost(Long id,Post post) {
        Post postToUpdate=postRepository.findById(id).get();
        postToUpdate.setContent(post.getContent());
        postToUpdate.setTitle(post.getTitle());

        postRepository.save(postToUpdate);
        return new ResponseEntity<>("Succesfully updated",HttpStatus.OK);
    }


}
